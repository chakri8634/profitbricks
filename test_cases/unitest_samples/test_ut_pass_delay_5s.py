import unittest
import time

class TestMathMethods(unittest.TestCase):

    def test_add(self):
        time.sleep(5)
        self.assertEqual(2+2,4)

    def test_sub(self):
        self.assertEqual(2-2,0)

    def test_mul(self):
        self.assertEqual(2*2,4)


if __name__ == '__main__':
    unittest.main()