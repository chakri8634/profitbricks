import unittest

class TestMathMethods(unittest.TestCase):

    def test_add(self):
        self.assertEqual(2+2,4)

    def test_sub(self):
        self.assertEqual(2-2,2)
        # self.assertNotEqual(2-2,1)

    def test_mul(self):
        self.assertEqual(2*2,4)


if __name__ == '__main__':
    unittest.main()