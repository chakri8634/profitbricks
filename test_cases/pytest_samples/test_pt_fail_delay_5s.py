import time

def inc(x):
    time.sleep(3)
    return x + 1

def dec(x):
    time.sleep(3)
    return x - 1

def test_answer1():
    assert inc(3) == 5

def test_answer2():
    assert dec(3) == 3