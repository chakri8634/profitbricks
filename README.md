# Running the pytest runner:
---

1. Prerequisite:
    a. Latest python 2.7.x version.
    b. pip and virtualenv is installed already
2. Unzip the ProfitBricks.zip or `git clone https://bitbucket.org/chakri8634/profitbricks.git`
3. Navigate into the directory using
    `cd ProfitBricks`
4. Initialize virtualenv using below command
    `virtualenv .`
6. Activate the virtualenv using below command
    `source bin/activate`
7. Install all the required libraries using pip as 
    `pip install -r requirements.txt`
8. Run the app by issuing below command
    `python app/app.py`
9. Open http://localhost:5000/