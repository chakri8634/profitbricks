from flask import Flask
from flask import render_template
from flask import request
from flask import jsonify
from flask_socketio import SocketIO
from flask_socketio import send, emit
from werkzeug import secure_filename
import os
from utils.resp_builder import HTTP_200,HTTP_400
from utils.efile import create_test_dir
from utils.db import create_test_run
from utils.test_runner import run_test
from flask_sqlalchemy import SQLAlchemy
import datetime
from utils.config import Config
from utils.executor import get_runner
from Queue import Queue
import threading
import time
from concurrent.futures import ThreadPoolExecutor
import subprocess
import glob
from flask import send_file
from sqlalchemy import desc
from collections import OrderedDict
import db.state as state



"""
    Flask app which accepts all request and return JSON reponse
"""
UPLOAD_FOLDER = os.path.join(os.getcwd(),'test_runs')
app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
print "UPLOAD_FOLDER",UPLOAD_FOLDER 
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite3'
socketio = SocketIO(app)
executor = None
# model for the run
db = SQLAlchemy(app=app)
state.db = db
# load after db is inittiated
from db.models import Status,TestLogResult,TestResult,TestRun

def process_data(request):
    requester = request.form.get("requester")
    request_id = request.form.get("request_id")
    return (requester,request_id)

# to get the first 30 lines of a log file for preview
def head(num_of_lines,log_file):
    head = []
    with open(log_file) as logfile:
        for _ in xrange(num_of_lines):
            try:
                head.append(next(logfile))
            except StopIteration:
                # break since number of lines are lesser than default 30
                break

    return head

def update_task_status(id,status):
    task = TestRun.query.get(id)
    task.status = status
    task.created_date = db.func.current_timestamp()
    db.session.commit()


def process_logs(test,data):
    print "Process logs: # ",data
    failed = False
    for test_item,test_stats in data.iteritems():
        print test_item,test_stats

        if test_stats[0]['results']['fail'] > 0:
            failed = True

        unique_file_name = "___".join(test.test_run_path.split("/")[-2:])+"___"+test_item
        try:
            test_result_prev = TestResult.query.get(unique_file_name)
        except Exception as ex:
            print ex
        print "test_result_prev => ",test_result_prev

        if test_result_prev:
            test_result = test_result_prev
            test_result.duration=test_stats[0]['results']['duration']
            test_result.fail=test_stats[0]['results']['fail']
            test_result.success=test_stats[0]['results']['success']
            test_result.total=test_stats[0]['results']['total']
            test_result.run_id = test.id
            test_result.error_message = test_stats[1]['message']
        else:
            test_result = TestResult(log_name=unique_file_name,\
                                    duration=test_stats[0]['results']['duration'],\
                                    fail=test_stats[0]['results']['fail'],\
                                    success=test_stats[0]['results']['success'],\
                                    total=test_stats[0]['results']['total'],\
                                    error_message = test_stats[1]['message'],\
                                    run_id = test.id)
        print "test result :",test_result
        # update test result log for showing last 5 transactions:
        log_result = TestLogResult(log_name=unique_file_name,issuccess = 0 if test_stats[0]['results']['fail'] > 0 else 1)
        db.session.add(log_result)
        db.session.add(test_result)
        db.session.commit()

        try:
            test_case = TestRun.query.get(test.id)
            if failed:
                test_case.status = Status.ERROR
            else:
                test_case.status = Status.SUCCESS
            db.session.add(test_result)
        except: pass
        db.session.commit()

def run_test_with_config(test):
    print "Executing task id # ",test.id
    update_task_status(test.id,Status.RUNNING)
    run_test(test,process_logs,test)

def thread_pool_executor():
    print "Initializing executor"
    global executor 
    executor = ThreadPoolExecutor(max_workers=10)


# Default home
@app.route("/",methods=["GET", "POST"])
def home():
    return render_template("index.html")

# for upload test cases into a test dir
@app.route("/test/upload",methods=["GET", "POST"])
def upload_test_cases():
    if request.method == 'POST':
      requester,req_id = process_data(request)
      files  = request.files.getlist("tests")
      dir_name = create_test_dir(app.config['UPLOAD_FOLDER'])
      for f in files:
        f.save(os.path.join(app.config['UPLOAD_FOLDER'],dir_name,secure_filename(f.filename)))
      test_run = TestRun(requester=requester,test_run_id=req_id,test_run_path=os.path.join(app.config['UPLOAD_FOLDER'],dir_name))
      db.session.add(test_run)
      db.session.commit()
      db.session.refresh(test_run)
      return HTTP_200()

# to list all tasks
@app.route("/tasks",methods=["GET"])
def get_tasks():
    if request.method == 'GET':
        return HTTP_200(data = [i.serialize for i in TestRun.query.all()])

# to create all tasks
@app.route("/tasks",methods=["POST"])
def submit_tasks():
    if request.method == 'POST':
        requester  = request.form.get("requester","")
        request_id = request.form.get("request_id","")
        test_config = request.form.get("test_config","")
        files = request.files.getlist("tests")
        dir_name = create_test_dir(app.config['UPLOAD_FOLDER'])

        test_stats = TestRun.query.filter(TestRun.test_run_id == request_id).all()
        # print "test stats ",test_stats
        if len(test_stats) > 1:
            return HTTP_400()
        for f in files:
            f.save(os.path.join(app.config['UPLOAD_FOLDER'],dir_name,secure_filename(f.filename)))
        
        test_run = TestRun(requester=requester,\
                        test_run_id=request_id,\
                        test_run_path = os.path.join(app.config['UPLOAD_FOLDER'],dir_name),\
                        test_run_with=test_config)
        db.session.add(test_run)
        db.session.commit()
        db.session.refresh(test_run)
        return HTTP_200()

# to delete a particular test task
@app.route("/tasks/<id>",methods=["DELETE"])
def delete_tasks(id):
    if request.method == 'DELETE':
        test_run = TestRun.query.get(id)
        print test_run.id
        db.session.delete(test_run)
        db.session.commit()
        return HTTP_200()

# to fetch trend data
@app.route("/tests/trend/<id>",methods=["GET"])
def test_trend(id):
    if request.method == 'GET':
        return HTTP_200(data = [i.serialize for i in TestRun.query.all()])

# to fetch full report of a task
@app.route("/tasks/full/<id>",methods=["GET"])
def get_full_info(id):
    data = TestRun.query.get(id)
    test_stats = TestResult.query.filter(TestResult.run_id == id).all()
    sum_success = sum([i.success for i in test_stats])
    sum_total = sum([i.total for i in test_stats])
    cumulative = {
        "success" : sum_success,
        "total"   : sum_total,
        "error"   : sum_total - sum_success
    }
    last_5_results = {}
    for test in test_stats:
        last_5_results[test.log_name] = [ log.issuccess for log in TestLogResult.query.filter(TestLogResult.log_name == test.log_name).order_by(desc(TestLogResult.created_date)).limit(5).all()]
    
    return render_template("test_complete_info.html",\
                            test_info=data,test_stats=test_stats,\
                            cumulative=cumulative,last_5_results=last_5_results)

# to fetch full report as JSON
@app.route("/tasks/full/<id>",methods=["POST"])
def get_full_info_json(id):
    data = TestRun.query.get(id)
    test_stats = TestResult.query.filter(TestResult.run_id == id).all()
    sum_success = sum([i.success for i in test_stats])
    sum_total = sum([i.total for i in test_stats])
    cumulative = {
        "success" : sum_success,
        "total"   : sum_total,
        "error"   : sum_total - sum_success
    }
    last_5_results = {}
    for test in test_stats:
        last_5_results[test.log_name] = [ log.issuccess for log in TestLogResult.query.filter(TestLogResult.log_name == test.log_name).order_by(desc(TestLogResult.created_date)).limit(5).all()]
    
    header = {
        "requester": data.requester,
        "test_run_id" : data.test_run_id,
        "created_date" : data.created_date,
        "status":data.status.value
    }

# jsonify(test_info=data.serialize,test_stats=[i.serialize for i in test_stats],\
#                             cumulative=cumulative,last_5_results=last_5_results)
    return HTTP_200(data = [i.serialize(last_5_results) for i in test_stats],header = header,cumulative = cumulative)

# fetch logs based on the task
@app.route("/tasks/logs/<id>",methods=["GET"])
def get_log_by_id(id):
    test_run = TestRun.query.get(id)
    log_dir = glob.glob(os.path.join(test_run.test_run_path,"logs","*.log"))
    data = []
    for log_file in log_dir:
        print "*"*10,log_file
        data.append({
            "file_name" : log_file.split(os.path.sep)[-1],
            "file_head" : head(num_of_lines = 30,log_file = log_file)
        })
    return HTTP_200(data = data)

# download a log file
@app.route("/file/download/<log>",methods=['GET'])
def download_file(log):
    return send_file(app.config['UPLOAD_FOLDER'] + os.path.sep + log.split("___")[1] + os.path.sep + "logs" + os.path.sep + log.split("___")[2],attachment_filename='python.log')

# # fetch logs
# @app.route("/logs",methods=["POST"])
# def get_logs():
#     if request.method == 'POST':
#         return HTTP_200(data = [i.serialize for i in TestRun.query.all()])

# Re run a specific task id
@app.route("/tasks/rerun/<id>",methods=["GET","POST"])
def re_run_task(id):
    # if request.method == 'POST':
    update_task_status(id,Status.SCHDEULED)
    return HTTP_200()

# Will inititalize all workers only after first request kicks in
@app.before_first_request
def before_first_request():
    def run_job():
        while True:
            print("Checking for scheduled jobs")
            tasks = TestRun.query.filter(TestRun.status == Status.SCHDEULED).all()
            for test_run in tasks:
                print "Submiting task id #",test_run.id
                executor.submit(run_test_with_config,test_run)
            time.sleep(3)
    
    thread_pool_executor()
    thread = threading.Thread(target=run_job)
    thread.start()


# @socketio.on('message')
# def handle_message(message):
#     print 'received message: '+message

if __name__ == "__main__":
    db.create_all()
    socketio.run(app,debug=True)