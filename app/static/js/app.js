// Configurations
POLLING_TIME = 2000

function displayHeader(url){
    $.post(url,function(res){
        var source = $("#test-detail-header").html(); 
        var template = Handlebars.compile(source);
        $('#header').empty()
        $('#header').append(template(res));
        
        var source = $("#test-detail-cumulative").html(); 
        var template = Handlebars.compile(source);
        $('#cumulative').empty()
        $('#cumulative').append(template(res));
    })
}


function doPoll(datatable){
    datatable.ajax.reload();
    displayHeader(window.location.pathname)
    // $("span.pie").peity("donut")
    setTimeout(doPoll,POLLING_TIME,datatable);
}

function showModal(data){
    console.log(data)
    var source = $("#document-template").html(); 
    var template = Handlebars.compile(source);
    $('body').append(template(data));
    
    //clear the old dimmers here
    $('.ui.dimmer').empty();
    $('.ui.accordion').accordion();
    $('.ui.modal').modal({closable:true}).modal('show');
}

function fetchAndDisplayLogs(url){
    console.log(url)
    $.get(url,function(data){
        showModal(data)
    });
}

function reRun(url){
    $.get(url,function(d){
        $.notify("Re-run requested successfully!!","info");
    });
}

function deleteTask(url){
    $.ajax({
        url: url,
        type: 'DELETE',
        success: function(result) {
            $.notify("Task deleted successfully","error");
        }
    });
}

function submitForm(){
    // window.location.reload()
}
/* since handled from app comment for now */
Handlebars.registerHelper('formatLog', function(test_log) {
    return test_log.join();
});

$("#taskForm").on('submit', function( e ) {
    e.preventDefault();
    if( $("#requester").val() == "" || 
        $("#request_id").val() == "" || 
        $("#test_config").val() == "" || 
        $("#file_list").text() == "")
        {
            $.notify("Form is incomplete","error");
            return;
        }else{
            $.ajax( {
                url: $(this).attr('action'),
                type: 'POST',
                data: new FormData( this ),
                processData: false,
                contentType: false,
                error: function(d,e){
                    $.notify("Can run only task with test environment id!!","error");
                }
            }).done(function( data ) {
                $.notify("Test run requested successfully!!","success");
                window.location.href = "/"
            });
        }
});


// reset form
$('#resetBtn').on("click",function(e){
    e.preventDefault();
    $('#taskForm').trigger("reset");
    $("#file_list").text("");
});

$(document).ready(function () {
    // var socket = io.connect('http://' + document.domain + ':' + location.port);
    // socket.on('connect', function () {
    //     socket.send('my event');
    // });

    // To show the file names next to the file uploader
    $('input[type="file"]').change(function(e){
        var files = e.target.files;
        var file_list = ""
        for(var i=0;i<files.length;i++){
            file_list += " , "+files[i].name
        }
        $("#file_list").text(file_list.substring(3,43))
    });

    $("span.pie").peity("donut");
    $('.ui.dropdown').dropdown();
    $('#submitBtn').on('click',submitForm);
    var base_url = "/tasks"
    if(window.location.href.indexOf("/full/") > -1){
        displayHeader(window.location.pathname)
        var datatable = $("#tasks").DataTable({
            "ajax" : {
                "url" : window.location.pathname,
                "type" : "POST"
            },
            "drawCallback": function( settings ) {
                $("span.pie").peity("donut");
                $(".popup").popup();
            },
            "columns" : [
                {"data":"log_name"},
                {"data":"duration"},
                {"data":"success","width":"2%"},
                {"data":"fail","width":"2%"},
                {"data":"error_message"},
                {"data":"trend","width":"15%"},
                {"data":"log_name","width":"20%"},
            ],
            "columnDefs":[
                {
                    render: function(data){
                        return data.split("___").slice(-1)[0].replace(".log","")
                    },
                    "targets" : 0
                },
                {
                    "render": function(data,type,row){
                        html_text = ""
                        for(var i=0;i<data.length;i++){
                            color = 'red'
                            if(data[i] == 1){
                                color = 'green'
                            }

                            html_text += '<i class="square icon '+color+'"></i>'
                        }
                        return html_text
                    },
                    "targets" : 5
                },
                {
                    "render": function(data,type,row){
                        return '<a target="blank" href="/file/download/'+data+'"><i class="file alternate icon"></i>View</a>&nbsp;&nbsp;<a target="blank" href="/file/download/'+data+'" download><i class="download icon"></i>Download</a>';
                    },
                    "targets" : 6
                }
            ]
        });
        console.log(datatable)
    }else{
        var datatable = $("#tasks").DataTable({
            "ajax" : base_url,
            "drawCallback": function( settings ) {
                $("span.pie").peity("donut");
                $(".popup").popup();
            },
            "columns" : [
                {"data":"id"},
                {"data":"request_id"},
                {"data":"requester"},
                {"data":"created"},
                {"data":"status"},
                {"data":"id"},
            ],
            "columnDefs":[
                {
                    "render": function(data,type,row){
                        return "<a href='/tasks/full/"+data+"'>"+data+"</a>";
                    },
                    "targets" : 0
                },
                {
                    "render": function(data,type,row){
                        return "<a class='popup' href='javascript:void(0);' onClick="+'"'+"fetchAndDisplayLogs('/tasks/logs/"+data+"')"+'"'+" data-content='Get complete logs of the run'>Logs</a>&nbsp;&nbsp;&nbsp;<a class='popup' href='javascript:void(0);' onClick="+'"'+"reRun('/tasks/rerun/"+data+"')"+'"'+" data-content='Re run a test case'>Re-Run</a>&nbsp;&nbsp;&nbsp;<a class='popup' href='javascript:void(0);' onClick="+'"'+"deleteTask('/tasks/"+data+"')"+'"'+" data-content='Delete a test task'>Delete</a>";
                    },
                    "targets" : 5
                }
            ]
        });
    }
    doPoll(datatable);
});