"""
Configuration class for holding all proerties of a task run
"""
class Config:
    id = ""
    requester = ""
    test_run_id = ""
    test_run_path = ""
    status = ""

    def __init__(self,id,requester,test_run_id,test_run_path,status=""):
        self.id = id
        self.requester = requester
        self.test_run_id = test_run_id
        self.test_run_path = test_run_path
        self.status = status