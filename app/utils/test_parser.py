from glob import glob
import os
import re

class IParser:
    """
        Interface IParser for mainting stats as common method
    """
    def stats(self): raise NotImplementedError
    # def passed(self): raise NotImplementedError
    # def failed(self): raise NotImplementedError
    # def duration(self): raise NotImplementedError

# class for unittest parser
class UnittestParser(IParser):
    """
        Unit test parser that implemens Iparser for parsing the logs
    """
    __log_location = None
    __data = []
    __parsed_data = None

    def __init__(self,log_dir):
        self.__log_location = log_dir
        self.__parsed_data = {}
        self.__process()

    def __process(self):
        """
            Private method that crawls the logs and push daa
        """
        os.chdir(self.__log_location)
        for log_file in glob("*.*"):
            file_data = ""
            with open(log_file,"r+") as file:
                file_data = file.readlines()
            self.__parsed_data[log_file] = []
            self.__parsed_data[log_file].append(self.total(file_data))
            self.__parsed_data[log_file].append(self.error_message(file_data))


    def duration(self,file_data):
        """
            Calculate total time taken to run the test case
        """
        duration = 0
        # regex to find the total duration
        m = re.search(r'\d+\.\d{3}s', "".join(file_data))
        try:
            return m.group(0)
        except Exception:
            return duration

    def error_message(self,file_data):
        """
            Get a single error message from the log.
            In case of multiple errors it will just return first error message.
        """
        err_message = ""
        for i in file_data:
            # will find the first failed error message
            if not i.strip().startswith("FAIL:"):
                continue
            else:
                err_message = i.strip()
                break
        return {
            "message" : err_message
        }

    def total(self,file_data):
        """
           Will caluclate total , passed and failed counter
        """
        total = 0
        success = 0
        fail = 0
        for i in file_data:
            if i.strip() != '':
                # find the ok which are passed test cases
                if i.strip().split("...")[1].strip().lower() == 'ok':
                    success += 1
                # find the fail which are failed test cases
                if i.strip().split("...")[1].strip().lower() == 'fail':
                    fail += 1
            else:
                break
            total += 1
        return {
            "results" : {
                "total" : total,
                "success" : success,
                "fail" : fail,
                "duration": self.duration(file_data)
            }
        }

    def stats(self):
        return self.__parsed_data


class PyTestParser(IParser):
    """
        Py.test crawler will implement and returns the stats
    """
    __log_location = None
    __data = []
    __parsed_data = None

    def __init__(self,log_dir):
        self.__log_location = log_dir
        self.__parsed_data = {}
        self.__process()

    def __process(self):
        """
            Private method that crawls the logs and push daa
        """
        os.chdir(self.__log_location)
        for log_file in glob("*.*"):
            file_data = ""
            with open(log_file,"r+") as file:
                file_data = file.readlines()
            self.__parsed_data[log_file] = []
            self.__parsed_data[log_file].append(self.total(file_data))
            self.__parsed_data[log_file].append(self.error_message(file_data))


    def duration(self,file_data):
        """
            return total time took to run the respective test case
        """
        duration = 0
        m = re.search(r'\d+\.\d{2}\sseconds', "".join(file_data))
        try:
            return m.group(0).split(" ")[0]
        except Exception as identifier:
            return duration

    def error_message(self,file_data):
        """
            Return the first error message that is countered
        """
        err_message = ""
        for i in file_data:
            # Check for the exception or error message in the log
            l = re.search(r"[A-Z].*(Error|Exception)",i)
            if l == None:
                continue
            else:
                err_message = i.strip()
                break
        return {
            "message" : err_message
        }

    def total(self,file_data):
        """
            return the total, passed and failed counter
        """
        success = 0
        fail = 0

        # search for the regex to find the failed
        failed = re.search(r'\d+\sfailed', file_data[-1])
        if failed != None:
            fail = failed.group(0).replace(" failed","")
        else:
            fail = 0
        
        # search for the regex to find the passed
        successed = re.search(r'\d+\spassed', file_data[-1])
        if successed != None:
            success = successed.group(0).replace(" passed","")
        else:
            success = 0

        return {
            "results" : {
                "total" : int(success) + int(fail),
                "success" : success,
                "fail" : fail,
                "duration": self.duration(file_data)
            }
        }

    def stats(self):
        return self.__parsed_data