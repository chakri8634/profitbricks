from flask import json
from flask import Response

"""
Reponse builder for building 2xx,4xx
"""

def HTTP_200(status="OK",msg="success",data=[],**kwargs):
  message_default = {"msg":msg,"status":status,"data":data}
  message = dict(message_default,**kwargs)
  response = Response(
          response=json.dumps(message),
          status=200,
          mimetype='application/json'
        )
  return response

def HTTP_400(status="OK",msg="success",data=[],**kwargs):
  message_default = {"msg":msg,"status":status,"data":data}
  message = dict(message_default,**kwargs)
  response = Response(
          response=json.dumps(message),
          status=400,
          mimetype='application/json'
        )
  return response