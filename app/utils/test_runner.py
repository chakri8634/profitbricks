import subprocess
import os
import glob
from subprocess import call,PIPE,STDOUT,Popen
from .test_parser import UnittestParser,PyTestParser

"""
    Test runner which will assign to the respective runner and parser for logs
"""

run_config = {
    "unittest" : "python {} -v",
    "pytest" : "pytest",
}


def run_test(test_config,callback,params):
    test_dir = test_config.test_run_path
    try:
        run_with = test_config.test_run_with
    except:
        run_with = "unittest"
    log_dir = os.path.join(test_dir,'logs')
    
    # creating logs directory if not present
    try:
        os.makedirs(log_dir)
    except:
        pass
        
    test_files = glob.glob(os.path.join(test_dir,"*.py"))
    os.chdir(test_dir)
    for test_file in test_files:
        print "Running test : ",os.path.basename(test_file)
        log_file = os.path.join(test_dir,"logs",os.path.basename(test_file)+".log")
        print "Log file => ",log_file
        
        # based on the test config run the respective runner
        if run_with == "unittest":
            p = Popen("python {} -v".format(test_file), shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT)
        elif run_with == "pytest":
            p = Popen("py.test {}".format(test_file), shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT)

        # dump the console output ot log file
        with open(log_file,"w+") as f:
            for line in p.stdout:
                f.write(line)
    
    # parse unittest to get results
    if run_with == "unittest":
        parser = UnittestParser(log_dir)
    elif run_with == "pytest":
        parser = PyTestParser(log_dir)
    print "=> total ",parser.stats()
    callback(params,parser.stats())