from concurrent.futures import ThreadPoolExecutor
from Queue import Queue
import time

class Executor:
    # Here will be the instance stored.
    __instance = None
    __queue = Queue()

    @staticmethod
    def getInstance():
        """ Static access method. """
        if Executor.__instance == None:
            Executor()
        return Executor.__instance 

    def __init__(self):
        """ Virtually private constructor. """
        if Executor.__instance != None:
            raise Exception("This class is a Executor!")
        else:
            Executor.__instance = self

    def run_test(self):
        print "Running test case"



def get_runner():
    return Executor.getInstance()