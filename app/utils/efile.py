import os
import uuid

"""
    Need to change the return statement of this function
"""
def create_test_dir(test_dir):
    dir_name = str(uuid.uuid4())
    test_run_dir = os.path.join(test_dir,dir_name)
    try:
        os.makedirs(test_run_dir)
    except Exception as ex:
        # TODO: need to log using logger
        pass
    return dir_name