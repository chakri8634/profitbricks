import enum
from flask_sqlalchemy import SQLAlchemy
import datetime
import state
from collections import OrderedDict

"""
    Models required to store the state of the application
"""
db = state.db

class Status(enum.Enum):
    SCHDEULED = "Schdeuled"
    RUNNING = "Running"
    SUCCESS = "Success"
    ERROR = "Fail"

class TestLogResult(db.Model):
    __tablename__ = "test_log_results"
    id = db.Column('id',db.Integer,primary_key = True)
    log_name = db.Column('log_name',db.Unicode)
    issuccess = db.Column('issuccess',db.Integer)
    created_date = db.Column('created_date', db.DateTime, default=datetime.datetime.now)
    @property
    def serialize(self):
        return {
            'id' : self.id,
            'log_name' : self.log_name,
            'issuccess' : self.issuccess,
            'created_date' : self.created_date.strftime('%Y-%m-%d %H:%M:%S')
        }


class TestResult(db.Model):
    __tablename__ = "test_results"
    log_name = db.Column('log_name',db.Unicode,primary_key = True)
    duration = db.Column('duration',db.Integer)
    total = db.Column('total',db.Integer)
    success = db.Column('success',db.Integer)
    fail = db.Column('fail',db.Integer)
    error_message = db.Column('error_message',db.Unicode)
    run_id = db.Column('run_id',db.Integer)
    
    def serialize(self,trend):
        return OrderedDict([
            ('log_name' ,  self.log_name),
            ('duration' , self.duration),
            ('total' , self.total),
            ('success' , self.success),
            ('fail' , self.fail),
            ('error_message',self.error_message),
            ('run_id',self.run_id),
            ('trend',trend[self.log_name])
        ])

class TestRun(db.Model):
    __tablename__ = "test_scheduler"
    id = db.Column('id',db.Integer,primary_key = True)
    requester = db.Column('requester',db.Unicode)
    test_run_id = db.Column('test_run_id',db.Integer)
    test_run_path = db.Column('test_run_path',db.Unicode)
    test_run_with = db.Column('test_run_with',db.Unicode)
    created_date = db.Column('created_date', db.DateTime, default=datetime.datetime.now)
    status = db.Column('status', db.Enum(Status),default=Status.SCHDEULED)

    @property
    def serialize(self):
        return {
            'id' : self.id,
            'requester' : self.requester,
            'request_id' : self.test_run_id,
            'created' : self.created_date.strftime('%Y-%m-%d %H:%M:%S'),
            'status' : self.status.value,
            'trend':self.test_run_id,
            'run_with' : self.test_run_with
        }